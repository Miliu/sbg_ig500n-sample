/** 
 *  \file    sbg_ig500n_imu_node.cpp
 *  \brief   Implementation of ROS node that interfaces with an SBG Systems IG-500N.
 *
 *  \author Emili Hernandez
 *  \date   March 2014
 *  \version $Revision $
 *
 *  CSIRO Autonomous Systems Laboratory
 *  Queensland Centre for Advanced Technologies
 *  PO Box 883, Kenmore, QLD 4069, Australia
 *
 *  (c) Copyright CSIRO 2014
 *
 *  All rights reserved, no part of this program may be used
 *  without explicit permission of CSIRO
 *
 */
 
#include <sbg_ig500n/sbg_ig500n.h>

int main( int argc, char **argv )
{
   ros::init(argc, argv, "sbg_ig500_node");

   ros::NodeHandle nh;

   SbgIG n(nh);
   n.spin();
  

   return 0;
}
