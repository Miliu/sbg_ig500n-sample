#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <tf/tf.h>

static sensor_msgs::Imu reading;
static double roll, pitch, yaw;
static tf::Quaternion q;
ros::Publisher pub_;


# define M_PI 3.14159265358979323846  /* pi */

void imuCallback(const sensor_msgs::ImuConstPtr &msg)
{
	reading.header = msg->header;
	reading.orientation_covariance = msg->orientation_covariance;
	reading.angular_velocity_covariance = msg->angular_velocity_covariance;
	reading.linear_acceleration_covariance = msg->linear_acceleration_covariance;

	tf::quaternionMsgToTF(msg->orientation, q);
	tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
	q.setRPY(pitch, roll, -yaw);
	tf::quaternionTFToMsg(q, reading.orientation);

	reading.angular_velocity.x = msg->angular_velocity.y;
	reading.angular_velocity.y = msg->angular_velocity.x;
	reading.angular_velocity.z = -msg->angular_velocity.z;

	reading.linear_acceleration.x = msg->linear_acceleration.y;
	reading.linear_acceleration.y = msg->linear_acceleration.x;
	reading.linear_acceleration.z = -msg->linear_acceleration.z;

	pub_.publish(reading);
}  


int main(int argc, char** argv)
{
	ros::init(argc, argv, "ned2enu");
	ros::NodeHandle nh;
	ros::NodeHandle nh_private("~");

	std::string in_topic, out_topic;
	nh_private.param<std::string>("in_topic", in_topic, "imu_ned");
	nh_private.param<std::string>("out_topic", out_topic, "imu");

	ros::Subscriber sub = nh.subscribe(in_topic, 10, &imuCallback);
	pub_ = nh.advertise<sensor_msgs::Imu>(out_topic, 100);

	ros::spin();
	return 0;
}

