/** 
 *  \file    sbg_ig500n_imu.cpp
 *  \brief   Implementation of ROS node that interfaces with an SBG Systems IG-500N.
 *
 *  \author Emili Hernandez
 *  \date   March 2014
 *  \version $Revision $
 *
 *  CSIRO Autonomous Systems Laboratory
 *  Queensland Centre for Advanced Technologies
 *  PO Box 883, Kenmore, QLD 4069, Australia
 *
 *  (c) Copyright CSIRO 2014
 *
 *  All rights reserved, no part of this program may be used
 *  without explicit permission of CSIRO
 *
 */

#include <sbg_ig500n/sbg_ig500n.h>
#include <geodesy/utm.h>

SbgIG::SbgIG(ros::NodeHandle h):
   nh_(h),
   nh_private_("~"),
   error_count_(0),
   slow_count_(0), 
   new_data_(0),
   running_(false),
   update_gps_(false),
   diagnostic_()
 {
   nh_private_.param("port", port_, std::string("/dev/ttyUSB0"));
   nh_private_.param("motion_profile", mp_filename_, std::string("~")); 
   nh_private_.param("frequency", freq_, 100);
   
   nh_private_.param("frame_id", frame_id_, std::string("ig500n"));
   nh_private_.param("imu_topic", imu_topic_, std::string("data_ned"));
   
   nh_private_.param("orientation_stdev", orientation_stdev_, 0.017); //1 deg accuracy
   nh_private_.param("linear_acceleration_stdev", linear_acceleration_stdev_, 0.098); // from GX2
   nh_private_.param("angular_velocity_stdev", angular_velocity_stdev_, 0.012); // from GX2
   
   nh_private_.param("latlon_stdev", xy_stdev_, 5.0); 
   nh_private_.param("altitude_stdev", altitude_stdev_, 5.0); 
   
   if(freq_ == 0)
      freq_ = 100;
   divider_ = 100 / freq_;

   desired_freq_ = freq_;
   freq_diag_ = new diagnostic_updater::FrequencyStatus(
      diagnostic_updater::FrequencyStatusParam(&desired_freq_, &desired_freq_, 0.05));

   // imu sensors
   double angular_velocity_covariance = angular_velocity_stdev_ * angular_velocity_stdev_;
   double orientation_covariance = orientation_stdev_ * orientation_stdev_;
   double linear_acceleration_covariance = linear_acceleration_stdev_ * linear_acceleration_stdev_;

   imu_reading_.header.frame_id = frame_id_;
   imu_reading_.linear_acceleration_covariance[0] = linear_acceleration_covariance;
   imu_reading_.linear_acceleration_covariance[4] = linear_acceleration_covariance;
   imu_reading_.linear_acceleration_covariance[8] = linear_acceleration_covariance;
   imu_reading_.angular_velocity_covariance[0] = angular_velocity_covariance;
   imu_reading_.angular_velocity_covariance[4] = angular_velocity_covariance;
   imu_reading_.angular_velocity_covariance[8] = angular_velocity_covariance;
   imu_reading_.orientation_covariance[0] = orientation_covariance;
   imu_reading_.orientation_covariance[4] = orientation_covariance;
   imu_reading_.orientation_covariance[8] = orientation_covariance;

   t0_reading_.header.frame_id = frame_id_;
   t1_reading_.header.frame_id = frame_id_;

   magnetometer_reading_.header.frame_id = frame_id_;
   gyro_temperature_reading_.header.frame_id = frame_id_;
 
   // imu sensors raw
   imu_raw_reading_.header.frame_id = frame_id_;
   
   t0_raw_reading_.header.frame_id = frame_id_;
   t1_raw_reading_.header.frame_id = frame_id_;

   magnetometer_raw_reading_.header.frame_id = frame_id_;
   gyro_temperature_raw_reading_.header.frame_id = frame_id_;

   // gps
   double xy_covariance = xy_stdev_ * xy_stdev_;
   double altitude_covariance = altitude_stdev_ * altitude_stdev_;

   gps_reading_.header.frame_id = frame_id_;
   gps_reading_.position_covariance[0] = xy_covariance;
   gps_reading_.position_covariance[4] = xy_covariance;
   gps_reading_.position_covariance[8] = altitude_covariance;
   gps_reading_.position_covariance_type = sensor_msgs::NavSatFix::COVARIANCE_TYPE_DIAGONAL_KNOWN;
   gps_reading_.status.service = sensor_msgs::NavSatStatus::SERVICE_GPS;

   gps_velocity_reading_.header.frame_id = frame_id_;

   // pressure
   pressure_reading_.header.frame_id = frame_id_;
   pressure_reading_.variance = 0; // Unknown

   // Kalman filter
   odometry_reading_.header.frame_id = frame_id_;

   // time
   time_since_reset_reading_.header.frame_id = frame_id_;
   time_since_reset_reading_.source = "internal";

   // other
   status_reading_.header.frame_id = frame_id_;
   status_reading_.accelerometer.assign(3, false);
   status_reading_.gyroscope.assign(3, false);


   loadOutputMask(mask_);
   setupPublishers();

   diagnostic_.add( *freq_diag_ );
   if(mask_.other & SBG_OUTPUT_DEVICE_STATUS)
      diagnostic_.add("IG500N status", this, &SbgIG::deviceStatus);
   else
      ROS_WARN("Status not select in the output mask. No status in the diagnostics will be provided either.");

   uint32_t mp_id = getMPID(mp_filename_);
   if(start() == 0)
   {   
      std::string info_str = getInfo();
      ROS_INFO("%s", info_str.c_str());
      usleep(100000);

      diagnostic_.setHardwareID(device_code_);
      if(mp_id_ != mp_id)
      {
         setMotionProfile(mp_filename_);
         freq_diag_->clear();
         usleep(3000000);

         if(start() == 0)
         {
            info_str = getInfo();
            ROS_INFO("%s", info_str.c_str());
         }
      }
   }
}


SbgIG::~SbgIG()
{
   stop();
}


std::string SbgIG::getInfo() 
{
   sbg::SbgErrorCode error;
   char code[32];
   sbg::uint32 id, firwmare, calibration, main_board, gps_board;
   sbg::uint32 mp_id, mp_version;
   std::stringstream sstm;

   error = sbg::sbgGetInfos(protocol_handle_ , code, &id, &firwmare, &calibration, &main_board, &gps_board);
   if (error == sbg::SBG_NO_ERROR)
   {
      error = sbg::sbgGetMotionProfileInfo(protocol_handle_, &mp_id, &mp_version);
      if (error_ == sbg::SBG_NO_ERROR)
      {
         device_code_ = code;
         mp_id_ = mp_id;
         mp_version_ = mp_version;

         sstm  << std::endl
         << "\tProduct code: " << code << std::endl
         << "\tDevice id: " << versionToString(id) << std::endl
         << "\tFirmware version: " << versionToString(firwmare) << std::endl
         << "\tCalibration data version: " << versionToString(calibration) << std::endl
         << "\tMain board version: " << versionToString(main_board) << std::endl
         << "\tGPS board version: " << versionToString(gps_board) << std::endl
         << "\tMotion profile id-version: " << mp_id << " - " << versionToString(mp_version) << std::endl;
      }
      else
      {
         ROS_ERROR("getInfo-sbgGetMotionProfileInfo: %s", error_code_str(error).c_str());
      }
   }
   else
   {
      ROS_ERROR("getInfo-sbgGetInfos: %s", error_code_str(error).c_str());
   }
 
   return sstm.str();
}
   

int SbgIG::spin()
{
   if(error_ != sbg::SBG_NO_ERROR)
      return -1;

   ros::Rate r(freq_ * 2); 
   while (!ros::isShuttingDown())
   {
      error_ = sbg::sbgProtocolContinuousModeHandle(protocol_handle_);
      // ROS_INFO("new data %d", new_data_);
      if(new_data_ > 0)
      {   
         formatDatum(&output_);
         publishDatum();
         diagnostic_.update();
      }
     
      ros::spinOnce();
      r.sleep();
   }
   return 0;
}


void SbgIG::triggerErrorCallback(sbg::SbgProtocolHandleInt *pHandler, sbg::SbgErrorCode error, void *pUsrArg)
{
   char msg[256];
   
   sbgComErrorToString(error, msg);
   ROS_ERROR("TriggerErrorCallback: %s", msg);
}


void SbgIG::triggerCB(sbg::SbgProtocolHandleInt *handle, sbg::uint32 triggerMask, sbg::SbgOutput *output_ptr, void *arg_ptr)
{
   SbgIG *this_ptr = (SbgIG*)arg_ptr;
   
   if((output_ptr) && (arg_ptr))
   {
      // if((triggerMask & SBG_TRIGGER_MAIN_LOOP_DIVIDER) || (triggerMask & SBG_TRIGGER_GPS_VELOCITY))
      // {
      //    memcpy(&this_ptr->output_, output_ptr, sizeof(sbg::SbgOutput));
      //    this_ptr->new_data_ = (triggerMask & SBG_TRIGGER_MAIN_LOOP_DIVIDER) | (triggerMask & SBG_TRIGGER_GPS_VELOCITY);
      //    // ROS_INFO("New data: %u", this_ptr->new_data_);
      // }

      if((triggerMask & SBG_TRIGGER_MAIN_LOOP_DIVIDER))
      {
         memcpy(&this_ptr->output_, output_ptr, sizeof(sbg::SbgOutput));
         this_ptr->new_data_ = (triggerMask & SBG_TRIGGER_MAIN_LOOP_DIVIDER);
         // ROS_INFO("New data: %u", this_ptr->new_data_);
      }
      if(triggerMask & SBG_TRIGGER_GPS_VELOCITY)
         this_ptr->update_gps_ = true;
          
      output_ptr = NULL;
   }
   else
   {
         this_ptr->new_data_ = 0;
         // ROS_INFO("NO new data");
   }
}


int SbgIG::formatDatum(sbg::SbgOutput *data)
{
   ros::Time t = ros::Time::now();

   imu_reading_.header.stamp = t;
   magnetometer_reading_.header.stamp = t;
   t0_reading_.header.stamp = t;
   t1_reading_.header.stamp = t;
   gyro_temperature_reading_.header.stamp = t;

   imu_raw_reading_.header.stamp = t;
   magnetometer_raw_reading_.header.stamp = t;
   t0_raw_reading_.header.stamp = t;
   t1_raw_reading_.header.stamp = t;
   gyro_temperature_raw_reading_.header.stamp = t;

   time_since_reset_reading_.header.stamp = t;
   utc_time_reading_.header.stamp = t;
   
   gps_reading_.header.stamp = t;
   gps_velocity_reading_.header.stamp = t;

   odometry_reading_.header.stamp = t;
   status_reading_.header.stamp = t;
      

   if(data->outputMask & SBG_OUTPUT_QUATERNION)
   {
      imu_reading_.orientation.x  = data->stateQuat[1];
      imu_reading_.orientation.y  = data->stateQuat[2];
      imu_reading_.orientation.z  = data->stateQuat[3];
      imu_reading_.orientation.w  = data->stateQuat[0];
   }
   if(data->outputMask & SBG_OUTPUT_GYROSCOPES)
   {
      imu_reading_.angular_velocity.x  = data->gyroscopes[0];
      imu_reading_.angular_velocity.y  = data->gyroscopes[1];
      imu_reading_.angular_velocity.z  = data->gyroscopes[2];
   }
   if(data->outputMask & SBG_OUTPUT_ACCELEROMETERS)
   {
      imu_reading_.linear_acceleration.x  = data->accelerometers[0];
      imu_reading_.linear_acceleration.y  = data->accelerometers[1];
      imu_reading_.linear_acceleration.z  = data->accelerometers[2];
   }
   if(data->outputMask & SBG_OUTPUT_MAGNETOMETERS)
   {
      //TODO: sbg units to Tesla
      magnetometer_reading_.magnetic_field.x = data->magnetometers[0];   
      magnetometer_reading_.magnetic_field.y = data->magnetometers[1];
      magnetometer_reading_.magnetic_field.z = data->magnetometers[2];  
   }
   if(data->outputMask & SBG_OUTPUT_TEMPERATURES)
   {
      t0_reading_.temperature = data->temperatures[0];
      t1_reading_.temperature = data->temperatures[1];
   }
   
   /*
   if(data->outputMask & SBG_OUTPUT_EULER)
   {

   }
   if(data->outputMask & SBG_OUTPUT_MATRIX)
   {

   }
   */

   if(data->outputMask & SBG_OUTPUT_GYROSCOPES_RAW)
   {
      imu_raw_reading_.angular_velocity.x  = data->gyroscopesRaw[0];
      imu_raw_reading_.angular_velocity.y  = data->gyroscopesRaw[1];
      imu_raw_reading_.angular_velocity.z  = data->gyroscopesRaw[2];

   }
   if(data->outputMask & SBG_OUTPUT_ACCELEROMETERS_RAW)
   {
      imu_raw_reading_.linear_acceleration.x  = data->accelerometersRaw[0];
      imu_raw_reading_.linear_acceleration.y  = data->accelerometersRaw[1];
      imu_raw_reading_.linear_acceleration.z  = data->accelerometersRaw[2];
   }
   if(data->outputMask & SBG_OUTPUT_MAGNETOMETERS_RAW)
   {
      magnetometer_raw_reading_.magnetic_field.x = data->magnetometersRaw[0];   
      magnetometer_raw_reading_.magnetic_field.y = data->magnetometersRaw[1];
      magnetometer_raw_reading_.magnetic_field.z = data->magnetometersRaw[2]; 
   }
   if(data->outputMask & SBG_OUTPUT_TEMPERATURES_RAW)
   {
      t0_raw_reading_.temperature = data->temperaturesRaw[0];
      t1_raw_reading_.temperature = data->temperaturesRaw[1];
   }
   
   if(data->outputMask & SBG_OUTPUT_TIME_SINCE_RESET)
   {
      uint32_t tmp = data->timeSinceReset; //in ms
      time_since_reset_reading_.time_ref.sec = tmp / 1000;
      time_since_reset_reading_.time_ref.nsec = (tmp - time_since_reset_reading_.time_ref.sec * 1000) * 1000;
   }
   if(data->outputMask & SBG_OUTPUT_DEVICE_STATUS)
   {
      // status_reading_.data = data->deviceStatus;
      fillIGStatus(data->deviceStatus);
   }
   if((data->outputMask & SBG_OUTPUT_GPS_POSITION) && (data->outputMask & SBG_OUTPUT_GPS_INFO))
   {
      gps_reading_.latitude  = data->gpsLatitude * 1e-7;
      gps_reading_.longitude = data->gpsLongitude * 1e-7;
      gps_reading_.altitude  = data->gpsAltitude / 1000.0;
      
      uint8_t gps_flags = data->gpsFlags;
      unsigned int gps_fix = static_cast<unsigned int>(data->gpsFlags & 0x03);
           
      if(gps_fix == 0 || gps_fix == 1)
         gps_reading_.status.status = sensor_msgs::NavSatStatus::STATUS_NO_FIX;
      else if(gps_fix > 1)
         gps_reading_.status.status = sensor_msgs::NavSatStatus::STATUS_FIX;
   }
   
   if(data->outputMask & SBG_OUTPUT_GPS_NAVIGATION)
   {
      // In NED frame
      gps_velocity_reading_.twist.twist.linear.x = data->gpsVelocity[0] / 100.0;
      gps_velocity_reading_.twist.twist.linear.y = data->gpsVelocity[1] / 100.0;
      gps_velocity_reading_.twist.twist.linear.z = data->gpsVelocity[2] / 100.0;
      gps_velocity_reading_.twist.twist.angular.z = data->gpsHeading * 1e-5 * sbg::PI /180.0;
   }
   if(data->outputMask & SBG_OUTPUT_GPS_ACCURACY)
   {
      // Too much drift according to SBG?
      double h_accuracy = data->gpsHorAccuracy / 1000.0;
      double v_accuracy = data->gpsVertAccuracy / 1000.0;
      gps_reading_.position_covariance[0] = h_accuracy;// * h_accuracy;
      gps_reading_.position_covariance[4] = h_accuracy;// * h_accuracy;
      gps_reading_.position_covariance[8] = v_accuracy;// * v_accuracy;
      gps_reading_.position_covariance_type = sensor_msgs::NavSatFix::COVARIANCE_TYPE_DIAGONAL_KNOWN;

      double speed_accuracy = data->gpsSpeedAccuracy / 100.0;
      double heading_accuracy = data->gpsHeadingAccuracy * 1e-5 * sbg::PI / 180.0;
      // ROS_INFO("GPS heading accuracy %u", data->gpsHeadingAccuracy);
      gps_velocity_reading_.twist.covariance[0] = speed_accuracy;// * speed_accuracy;
      gps_velocity_reading_.twist.covariance[7] = speed_accuracy;// * speed_accuracy;
      gps_velocity_reading_.twist.covariance[14] = speed_accuracy;// * speed_accuracy;
      gps_velocity_reading_.twist.covariance[35] = heading_accuracy;// * heading_accuracy;
      
   }
    
   /*
   if(data->outputMask & SBG_OUTPUT_BARO_ALTITUDE)
   {

   }
   */
   if(data->outputMask & SBG_OUTPUT_BARO_PRESSURE)
   {
      pressure_reading_.fluid_pressure = data->baroPressure;
   }
   if(data->outputMask & SBG_OUTPUT_POSITION)
   {
      geodesy::UTMPoint utm;
     
      // North East Height (deg, deg, m) into UTM (m, m, m)
      fromMsg(geodesy::toMsg(data->position[0], data->position[1], data->position[2]), utm);
      odometry_reading_.pose.pose.position.x = utm.easting;
      odometry_reading_.pose.pose.position.y = utm.northing;
      odometry_reading_.pose.pose.position.z = utm.altitude;

      // WARNING: attitude from IMU (duplicating data values)
      odometry_reading_.pose.pose.orientation.x  = data->stateQuat[1];
      odometry_reading_.pose.pose.orientation.y  = data->stateQuat[2];
      odometry_reading_.pose.pose.orientation.z  = data->stateQuat[3];
      odometry_reading_.pose.pose.orientation.w  = data->stateQuat[0];
   }
   if(data->outputMask & SBG_OUTPUT_VELOCITY)
   {
      odometry_reading_.twist.twist.linear.x = data->velocity[0];
      odometry_reading_.twist.twist.linear.y = data->velocity[1];
      odometry_reading_.twist.twist.linear.z = data->velocity[2];

      // WARNING: angular twist from IMU (duplicating data values)
      odometry_reading_.twist.twist.angular.x  = data->gyroscopes[0];
      odometry_reading_.twist.twist.angular.y  = data->gyroscopes[1];
      odometry_reading_.twist.twist.angular.z  = data->gyroscopes[2];

   }
   if(data->outputMask & SBG_OUTPUT_ATTITUDE_ACCURACY)
   {
      odometry_reading_.pose.covariance[21] = data->attitudeAccuracy * data->attitudeAccuracy;
      odometry_reading_.pose.covariance[28] = data->attitudeAccuracy * data->attitudeAccuracy;
      odometry_reading_.pose.covariance[35] = data->attitudeAccuracy * data->attitudeAccuracy;
   }
   if(data->outputMask & SBG_OUTPUT_NAV_ACCURACY)
   {
      odometry_reading_.pose.covariance[0] = data->positionAccuracy * data->positionAccuracy;
      odometry_reading_.pose.covariance[7] = data->positionAccuracy * data->positionAccuracy;
      odometry_reading_.pose.covariance[14] = data->positionAccuracy * data->positionAccuracy;

      odometry_reading_.twist.covariance[0] = data->velocityAccuracy * data->velocityAccuracy;
      odometry_reading_.twist.covariance[7] = data->velocityAccuracy * data->velocityAccuracy;
      odometry_reading_.twist.covariance[14] = data->velocityAccuracy * data->velocityAccuracy;
   }
   
   if(data->outputMask & SBG_OUTPUT_GYRO_TEMPERATURES)
   {
      gyro_temperature_reading_.point.x = data->gyroTemperatures[0];
      gyro_temperature_reading_.point.y = data->gyroTemperatures[1];
      gyro_temperature_reading_.point.z = data->gyroTemperatures[2];
   }
   if(data->outputMask & SBG_OUTPUT_GYRO_TEMPERATURES_RAW)
   {
      gyro_temperature_raw_reading_.point.x = data->gyroTemperaturesRaw[0];
      gyro_temperature_raw_reading_.point.y = data->gyroTemperaturesRaw[1];
      gyro_temperature_raw_reading_.point.z = data->gyroTemperaturesRaw[2];
   }

   
   if(data->outputMask & SBG_OUTPUT_UTC_TIME_REFERENCE)
   {
      struct tm utc;     
      utc.tm_year = data->utcYear + 2000 - 1900;
      utc.tm_mon = data->utcMonth - 1;
      utc.tm_mday = data->utcDay;
      utc.tm_hour = data->utcHour;
      utc.tm_min = data->utcMin;
      utc.tm_sec = data->utcSec;

      utc_time_reading_.time_ref.sec = mktime(&utc);
      utc_time_reading_.time_ref.nsec = data->utcNano;
   }

   /*
   if(data->outputMask & SBG_OUTPUT_MAG_CALIB_DATA)
   {

   }
   if(data->outputMask & SBG_OUTPUT_GPS_TRUE_HEADING) //IG-500E only
   {

   }
   if(data->outputMask & SBG_OUTPUT_ODO_VELOCITIES)
   {

   }
   if(data->outputMask & SBG_OUTPUT_DELTA_ANGLES)
   {

   }
   if(data->outputMask & SBG_OUTPUT_HEAVE)
   {

   }*/
}

int SbgIG::publishDatum()
{
   // sensors
   if((mask_.sensors & SBG_OUTPUT_QUATERNION) || (mask_.sensors & SBG_OUTPUT_GYROSCOPES) || 
      (mask_.sensors & SBG_OUTPUT_ACCELEROMETERS))
      imu_pub_.publish(imu_reading_);

   if(mask_.sensors & SBG_OUTPUT_TEMPERATURES)
   {
      t0_pub_.publish(t0_reading_);
      t1_pub_.publish(t1_reading_);
   }
   
   if(mask_.sensors & SBG_OUTPUT_MAGNETOMETERS)
      magnetometer_pub_.publish(magnetometer_reading_);
   
   if(mask_.sensors & SBG_OUTPUT_GYRO_TEMPERATURES)
       gyro_temperature_pub_.publish(gyro_temperature_reading_);


   // raw sensors
   if((mask_.sensors_raw & SBG_OUTPUT_GYROSCOPES_RAW) || (mask_.sensors_raw & SBG_OUTPUT_ACCELEROMETERS_RAW))
      imu_raw_pub_.publish(imu_raw_reading_);      
      
   if(mask_.sensors_raw & SBG_OUTPUT_TEMPERATURES_RAW)
   {   
      t0_raw_pub_.publish(t0_raw_reading_);
      t1_raw_pub_.publish(t1_raw_reading_);
   }

   if(mask_.sensors_raw & SBG_OUTPUT_MAGNETOMETERS_RAW)
      magnetometer_raw_pub_.publish(magnetometer_raw_reading_);
   
   if(mask_.sensors_raw & SBG_OUTPUT_GYRO_TEMPERATURES_RAW)
      gyro_temperature_raw_pub_.publish(gyro_temperature_raw_reading_);


   // barometric
   if(mask_.barometric & SBG_OUTPUT_BARO_PRESSURE)
      pressure_pub_.publish(pressure_reading_);


   // Kalman filter
   if((mask_.kf & SBG_OUTPUT_POSITION) || (mask_.kf & SBG_OUTPUT_VELOCITY) || (mask_.kf & SBG_OUTPUT_HEAVE) || 
      (mask_.kf & SBG_OUTPUT_ATTITUDE_ACCURACY) || (mask_.kf & SBG_OUTPUT_NAV_ACCURACY))
      odometry_pub_.publish(odometry_reading_);
   

   // other
   if(mask_.other & SBG_OUTPUT_DEVICE_STATUS)
      status_pub_.publish(status_reading_);


   // time
   if(mask_.time & SBG_OUTPUT_TIME_SINCE_RESET)
      time_pub_.publish(time_since_reset_reading_);
      
   if(mask_.time & SBG_OUTPUT_UTC_TIME_REFERENCE)
      utc_time_pub_.publish(utc_time_reading_);


   if(update_gps_)
   {
      // gps
      if((mask_.gps & SBG_OUTPUT_GPS_POSITION) || (mask_.gps & SBG_OUTPUT_GPS_INFO))
         gps_pub_.publish(gps_reading_);

      if(mask_.gps & SBG_OUTPUT_GPS_NAVIGATION)
         gps_velocity_pub_.publish(gps_velocity_reading_);

      update_gps_ = false;
   }

   freq_diag_->tick();
   new_data_ = 0;
   return(0);
}


void SbgIG::setMotionProfile(const std::string filename)
{
   std::ifstream file(filename.c_str(),  std::ios::in | std::ios::binary);

   if(file.is_open())
   {
      std::filebuf* pbuf = file.rdbuf();

      // get file size using buffer's members
      std::size_t size = pbuf->pubseekoff (0, file.end, file.in);
      pbuf->pubseekpos (0, file.in);

      // allocate memory and fill it with file data
      char* buffer = new char[size];
      pbuf->sgetn (buffer, size);
      file.close();
      
      if (sbg::sbgSetMotionProfile(protocol_handle_, buffer, size) == sbg::SBG_NO_ERROR)
      {
         // Reboot is done by the device
         ROS_INFO("Motion profile successfully uploaded and applied.");
      }
      else
      {
         ROS_ERROR("setMotionProfile: Unable to upload the motion profile.");
      }
      delete[] buffer;
   }
   else
   {
      ROS_ERROR("setMotionProfile: Can not open %s", filename.c_str());
   }
}

int SbgIG::start()
{
   if (connect(port_, &protocol_handle_) != sbg::SBG_NO_ERROR)
      return -1;
  
   // combine mask for triggers
   sbg::uint32 mask_trigger0 =   mask_.sensors |
                                 mask_.sensors_raw |
                                 mask_.kf |
                                 mask_.barometric |
                                 mask_.time |
                                 mask_.other |
                                 mask_.gps;
   sbg::uint32 mask_trigger1 = mask_.gps;
 

   // 1st trigger                           
   error_ = sbg::sbgSetTriggeredMode(protocol_handle_, 0, SBG_TRIGGER_MAIN_LOOP_DIVIDER, mask_trigger0);                           
   if (error_ == sbg::SBG_NO_ERROR)
   {
      // 2nd trigger
      error_ = sbg::sbgSetTriggeredMode(protocol_handle_, 1, SBG_TRIGGER_GPS_VELOCITY, mask_trigger1);
      if (error_ == sbg::SBG_NO_ERROR)
      {
         error_ = sbg::sbgSetContinuousMode(protocol_handle_, sbg::SBG_TRIGGERED_MODE_ENABLE, divider_);
         // error_ = sbg::sbgSetContinuousMode(protocol_handle_, sbg::SBG_CONTINUOUS_MODE_ENABLE, 1);
         if (error_ == sbg::SBG_NO_ERROR)
         {
            error_ = sbgSetContinuousErrorCallback(protocol_handle_, triggerErrorCallback, NULL);
            if (error_ == sbg::SBG_NO_ERROR)
            {   
               error_ = sbgSetTriggeredModeCallback(protocol_handle_, triggerCB, this);//&output_);
               if (error_ == sbg::SBG_NO_ERROR)
               {
                  running_ = true;
               }  
               else
               {
                  ROS_ERROR("SetContinuousModeCallback: %s", error_code_str(error_).c_str());
               }
            }
            else
            {   
               ROS_ERROR("SetContinuousErrorCallback: %s", error_code_str(error_).c_str());
            }
         }
         else
         {
            ROS_ERROR("sbgSetContinuousMode: %s", error_code_str(error_).c_str());
         }
      }
      else
      {
         ROS_ERROR("sbgSetTriggeredMode2: %s", error_code_str(error_).c_str());
      }
   }
   else
   {
      ROS_ERROR("sbgSetTriggeredMode1: %s", error_code_str(error_).c_str());
   }
   
   if(error_ == sbg::SBG_NO_ERROR)
      return 0;
   return -1;
}


void SbgIG::stop()
{
   if(running_)
   {
      error_ = sbg::sbgProtocolClose(protocol_handle_);
      if(error_ != sbg::SBG_NO_ERROR)
      {         
         error_count_++;
         ROS_WARN("Error while stopping: %d", error_);
      }
      running_ = false;
   }
}


sbg::SbgErrorCode SbgIG::connect(const std::string port, sbg::SbgProtocolHandle *handle)
{
   // allowed baudrated of the sensor sorte according how like would be to used them
   static const int arr[] = {115200, 230400, 460800, 921600, 57600, 38400, 19200, 9600};
   std::vector<sbg::uint32> baudrate (arr, arr + sizeof(arr) / sizeof(arr[0]));
   sbg::SbgErrorCode error;

   std::vector<sbg::uint32>::iterator i = baudrate.begin();
   do
   {
      error = sbg::sbgComInit(port.c_str(), *i, handle);
      if(error != sbg::SBG_NO_ERROR)
         ROS_WARN("Attempt to connect at %s @ %d failed.", port.c_str(), *i);
      ++i;
   }while( error != sbg::SBG_NO_ERROR && i != baudrate.end() );

   if (error != sbg::SBG_NO_ERROR)
   {
      ROS_ERROR("Cannot connect to IG: %s", error_code_str(error).c_str());
      diagnostic_.broadcast(diagnostic_msgs::DiagnosticStatus::ERROR, "Cannot connect to IG");
   }

   return error;
}


std::string SbgIG::error_code_str(sbg::SbgErrorCode error) const
{
   char msg[256];
   sbgComErrorToString(error, msg);
   return std::string(msg);
}


std::string SbgIG::versionToString(const sbg::uint32 version)
{
   int v[4];

   v[0] = ((version >> 24) & 0xFF);
   v[1] = ((version >> 16) & 0xFF);
   v[2] = ((version >> 8) & 0xFF);
   v[3] = (version & 0xFF);

   std::stringstream sstm;
   sstm << v[0] << "." << v[1] << "." << v[2] << "."  << v[3];
   return sstm.str();
}


int SbgIG::getMPID(const std::string s)
{
   std::string str(s);
   
   size_t pos0 = str.rfind("/");
   size_t pos1 = str.find("_", pos0 + 1);
   size_t pos2 = str.find("_", pos1 + 1);
   std::string token = str.substr(pos1 + 1, pos2 - pos1 - 1);
   return atoi(token.c_str()); // Not C++11 std in ROS yet
}


void SbgIG::fillIGStatus(const uint32_t status)
{
   // Device
   (status & SBG_CALIB_INIT_STATUS_MASK) == SBG_CALIB_INIT_STATUS_MASK ? 
      status_reading_.calibration = true : status_reading_.calibration = false; 
   
   (status & SBG_SETTINGS_INIT_STATUS_MASK ) == SBG_SETTINGS_INIT_STATUS_MASK ?
      status_reading_.settings = true : status_reading_.settings = false;
  
   (status &  SBG_PROTOCOL_OUTPUT_STATUS_MASK) == SBG_PROTOCOL_OUTPUT_STATUS_MASK ?
      status_reading_.protocol = true : status_reading_.protocol = false;
  
   (status &  SBG_ALTI_INIT_STATUS_BIT_MASK) == SBG_ALTI_INIT_STATUS_BIT_MASK ?
      status_reading_.altimeter = true : status_reading_.altimeter = false;
  
   (status &  SBG_GPS_STATUS_BIT_MASK) == SBG_GPS_STATUS_BIT_MASK ?
      status_reading_.gps = true : status_reading_.gps = false;
  
  
   // Sensors
   (status &  SBG_ACCEL_0_SELF_TEST_STATUS_MASK) == SBG_ACCEL_0_SELF_TEST_STATUS_MASK ?
      status_reading_.accelerometer[0] = true : status_reading_.accelerometer[0] = false;

   (status &  SBG_ACCEL_1_SELF_TEST_STATUS_MASK) == SBG_ACCEL_1_SELF_TEST_STATUS_MASK ?
      status_reading_.accelerometer[1] = true : status_reading_.accelerometer[1] = false;

   (status &  SBG_ACCEL_2_SELF_TEST_STATUS_MASK) == SBG_ACCEL_2_SELF_TEST_STATUS_MASK ?
      status_reading_.accelerometer[2] = true : status_reading_.accelerometer[2] = false;

   (status &  SBG_ACCEL_RANGE_STATUS_MASK) == SBG_ACCEL_RANGE_STATUS_MASK ?
      status_reading_.accelerometers_range = true : status_reading_.accelerometers_range = false;

   (status &  SBG_GYRO_0_SELF_TEST_STATUS_MASK) == SBG_GYRO_0_SELF_TEST_STATUS_MASK ?
      status_reading_.gyroscope[0] = true : status_reading_.gyroscope[0] = false;

   (status &  SBG_GYRO_1_SELF_TEST_STATUS_MASK) == SBG_GYRO_1_SELF_TEST_STATUS_MASK ?
      status_reading_.gyroscope[1] = true : status_reading_.gyroscope[1] = false;

   (status &  SBG_GYRO_2_SELF_TEST_STATUS_MASK) == SBG_GYRO_2_SELF_TEST_STATUS_MASK ?
      status_reading_.gyroscope[2] = true : status_reading_.gyroscope[2] = false;

   (status &  SBG_GYRO_RANGE_STATUS_MASK) == SBG_GYRO_RANGE_STATUS_MASK ?
      status_reading_.gyroscopes_range = true : status_reading_.gyroscopes_range = false;

   (status &  SBG_MAG_CALIBRATION_STATUS_MASK) == SBG_MAG_CALIBRATION_STATUS_MASK ?
      status_reading_.magnetometer = true : status_reading_.magnetometer = false;
      
        
   // Measuremetes
   (status &  SBG_G_MEASUREMENT_VALID_MASK) == SBG_G_MEASUREMENT_VALID_MASK ?
      status_reading_.gravity = true : status_reading_.gravity = false;
      
   (status &  SBG_HEADING_MEASUREMENT_VALID_MASK) == SBG_HEADING_MEASUREMENT_VALID_MASK ?
      status_reading_.heading = true : status_reading_.heading = false;
      
   (status &  SBG_VEL_MEASUREMENT_VALID_MASK) == SBG_VEL_MEASUREMENT_VALID_MASK ?
      status_reading_.velocity = true : status_reading_.velocity = false;

   (status &  SBG_POS_MEASUREMENT_VALID_MASK) == SBG_POS_MEASUREMENT_VALID_MASK ?
      status_reading_.position = true : status_reading_.position = false;


   // UTC
   (status &  SBG_UTC_VALID_MASK) == SBG_UTC_VALID_MASK ?
      status_reading_.utc = true : status_reading_.utc = false;

   (status &  SBG_UTC_ROUGH_ACCURACY_MASK) == SBG_UTC_ROUGH_ACCURACY_MASK ?
      status_reading_.utc_rough_estimation = true : status_reading_.utc_rough_estimation = false;

   (status &  SBG_UTC_FINE_ACURACY_MASK) == SBG_UTC_FINE_ACURACY_MASK ?
      status_reading_.utc_synchronization = true : status_reading_.utc_synchronization = false;
}


void SbgIG::deviceStatus(diagnostic_updater::DiagnosticStatusWrapper &status)
{
   int error = 0;
  
   // Device
   if(status_reading_.calibration)
      status.add("Calibration", "ok");
   else
   {
      status.add("Calibration", "no");
      error++;
   }

   if(status_reading_.settings)
      status.add("Settings", "ok");
   else
   {   
      status.add("Settings", "no");
      error++;
   }

   if(status_reading_.protocol)
      status.add("Protocol", "ok");
   else
   {
      status.add("Protocol", "no");
      error++;
   }

   if(status_reading_.altimeter)
      status.add("Altimeter", "ok");
   else
   {
      status.add("Altimeter", "no");
      error++;
   }
   
   if(status_reading_.gps)
      status.add("GPS", "ok");
   else
   {
      status.add("GPS", "no");
      error++;
   }

   if(error > 0) 
      status.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "IG not working properly");
   else if(!running_)
      status.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "IG not running");
   // else if high latency!
      // status.summary(1, "Excessive delay");
   else 
      status.summary(diagnostic_msgs::DiagnosticStatus::OK, "IG is running");
}


void SbgIG::loadOutputMask(sbg::OutputMask& m)
{
   bool tmp;
   sbg::uint32 sensors_mask, sensors_raw_mask, gps_mask, kf_mask, barometric_mask, time_mask, other_mask; 
  
   sensors_mask = 0;
   nh_private_.param("quaternion", tmp, true);
   if(tmp)
      sensors_mask |= SBG_OUTPUT_QUATERNION;

   nh_private_.param("euler", tmp, false);
   if(tmp)
      sensors_mask |= SBG_OUTPUT_EULER;
   
   nh_private_.param("matrix", tmp, false);
   if(tmp)
      sensors_mask |= SBG_OUTPUT_MATRIX;
   
   nh_private_.param("gyroscopes", tmp, true);
   if(tmp)
      sensors_mask |= SBG_OUTPUT_GYROSCOPES;
   
   nh_private_.param("temperatures", tmp, false);
   if(tmp)
      sensors_mask |= SBG_OUTPUT_TEMPERATURES;

    nh_private_.param("gyro_temperatures", tmp, false);
   if(tmp)
      sensors_mask |= SBG_OUTPUT_GYRO_TEMPERATURES;
   
   nh_private_.param("magnetometers", tmp, false);
   if(tmp)
      sensors_mask |= SBG_OUTPUT_MAGNETOMETERS;
   
   nh_private_.param("accelerometers", tmp, true);
   if(tmp)
      sensors_mask |= SBG_OUTPUT_ACCELEROMETERS;
   

   sensors_raw_mask = 0;
   nh_private_.param("gyroscopes_raw", tmp, false);
      if(tmp)
      sensors_raw_mask |= SBG_OUTPUT_GYROSCOPES_RAW;

   nh_private_.param("accelerometers_raw", tmp, false);
   if(tmp)
      sensors_raw_mask |= SBG_OUTPUT_ACCELEROMETERS_RAW;

   nh_private_.param("magnetometers_raw", tmp, false);
   if(tmp)
      sensors_raw_mask |= SBG_OUTPUT_MAGNETOMETERS_RAW;

   nh_private_.param("temperatures_raw", tmp, false);
   if(tmp)
      sensors_raw_mask |= SBG_OUTPUT_TEMPERATURES_RAW;

   nh_private_.param("gyro_temperatures_raw", tmp, false);
   if(tmp)
      sensors_raw_mask |= SBG_OUTPUT_GYRO_TEMPERATURES_RAW;

   
   time_mask = 0;
   nh_private_.param("time_since_reset", tmp, true);
   if(tmp)
      time_mask |= SBG_OUTPUT_TIME_SINCE_RESET;

   nh_private_.param("utc_time_reference", tmp, false);
   if(tmp)
      time_mask |= SBG_OUTPUT_UTC_TIME_REFERENCE;


   gps_mask = 0;
   nh_private_.param("gps_position", tmp, false);
   if(tmp)
      gps_mask |= SBG_OUTPUT_GPS_POSITION;
   
   nh_private_.param("gps_navigation", tmp, false);
   if(tmp)
      gps_mask |= SBG_OUTPUT_GPS_NAVIGATION;

   nh_private_.param("gps_true_heading", tmp, false); //IG-500E only
   if(tmp)
      gps_mask |= SBG_OUTPUT_GPS_TRUE_HEADING;

   nh_private_.param("gps_accuracy", tmp, false);
   if(tmp)
      gps_mask |= SBG_OUTPUT_GPS_ACCURACY;

   nh_private_.param("gps_info", tmp, false);
   if(tmp)
      gps_mask |= SBG_OUTPUT_GPS_INFO;

   
   barometric_mask = 0;
   nh_private_.param("baro_altitude", tmp, false);
   if(tmp)
      barometric_mask |= SBG_OUTPUT_BARO_ALTITUDE;

   nh_private_.param("baro_pressure", tmp, false);
   if(tmp)
      barometric_mask |= SBG_OUTPUT_BARO_PRESSURE;


   kf_mask = 0;
   nh_private_.param("position", tmp, false);
   if(tmp)
      kf_mask |= SBG_OUTPUT_POSITION;

   nh_private_.param("velocity", tmp, false);
   if(tmp)
      kf_mask |= SBG_OUTPUT_VELOCITY;

   nh_private_.param("heave", tmp, false);
   if(tmp)
      kf_mask |= SBG_OUTPUT_HEAVE;

   nh_private_.param("attitude_accuracy", tmp, false);
   if(tmp)
      kf_mask |= SBG_OUTPUT_ATTITUDE_ACCURACY;

   nh_private_.param("nav_accuracy", tmp, false);
   if(tmp)
      kf_mask |= SBG_OUTPUT_NAV_ACCURACY;

   
   other_mask = 0;   
   nh_private_.param("device_status", tmp, true);
   if(tmp)
      other_mask |= SBG_OUTPUT_DEVICE_STATUS;

   nh_private_.param("mag_calib_data", tmp, false);
   if(tmp)
      other_mask |= SBG_OUTPUT_MAG_CALIB_DATA;

   nh_private_.param("odo_velocity", tmp, false); //IG-500E only
   if(tmp)
      other_mask |= SBG_OUTPUT_ODO_VELOCITIES;

   nh_private_.param("delta_angles", tmp, false);
   if(tmp)
      other_mask |= SBG_OUTPUT_DELTA_ANGLES;


   sbg::uint32 mask = sensors_mask | sensors_raw_mask | gps_mask | kf_mask | barometric_mask;
   if (mask == 0)
   {
      ROS_WARN("No ouput mask detected. Loading basic mask");
      sensors_mask = SBG_OUTPUT_QUATERNION |
                     SBG_OUTPUT_GYROSCOPES |
                     SBG_OUTPUT_ACCELEROMETERS |
                     SBG_OUTPUT_MAGNETOMETERS |
                     SBG_OUTPUT_TIME_SINCE_RESET |
                     SBG_OUTPUT_DEVICE_STATUS;
   }

   m.sensors = sensors_mask;
   m.sensors_raw = sensors_raw_mask;
   m.gps = gps_mask;
   m.kf = kf_mask;
   m.barometric = barometric_mask;
   m.time = time_mask;
   m.other = other_mask;   
}


void SbgIG::setupPublishers()
{
   // sensors
   if((mask_.sensors & SBG_OUTPUT_QUATERNION) || (mask_.sensors & SBG_OUTPUT_GYROSCOPES) || 
      (mask_.sensors & SBG_OUTPUT_ACCELEROMETERS))
      imu_pub_ = nh_.advertise<sensor_msgs::Imu>("imu/" + imu_topic_, freq_);
   
   if(mask_.sensors & SBG_OUTPUT_TEMPERATURES)
   {
      t0_pub_ = nh_.advertise<sensor_msgs::Temperature>("imu/temperature0", freq_);
      t1_pub_ = nh_.advertise<sensor_msgs::Temperature>("imu/temperature1", freq_);
   }
   
   if(mask_.sensors & SBG_OUTPUT_MAGNETOMETERS)
      magnetometer_pub_ = nh_.advertise<sensor_msgs::MagneticField> ("imu/magnetometer", freq_);
   
   if(mask_.sensors & SBG_OUTPUT_GYRO_TEMPERATURES)
      gyro_temperature_pub_ = nh_.advertise<geometry_msgs::PointStamped> ("imu/gyro_temperature", freq_);


   // raw sensors
   if((mask_.sensors_raw & SBG_OUTPUT_GYROSCOPES_RAW) || (mask_.sensors_raw & SBG_OUTPUT_ACCELEROMETERS_RAW))
      imu_raw_pub_ = nh_.advertise<sensor_msgs::Imu>("imu_raw/" + imu_topic_, freq_);
   
   if(mask_.sensors_raw & SBG_OUTPUT_TEMPERATURES_RAW)
   {   
      t0_raw_pub_ = nh_.advertise<sensor_msgs::Temperature>("imu_raw/temperature0", freq_);
      t1_raw_pub_ = nh_.advertise<sensor_msgs::Temperature>("imu_raw/temperature1", freq_);
   }

   if(mask_.sensors_raw & SBG_OUTPUT_MAGNETOMETERS_RAW)
      magnetometer_raw_pub_ = nh_.advertise<sensor_msgs::MagneticField> ("imu_raw/magnetometer", freq_);

   if(mask_.sensors_raw & SBG_OUTPUT_GYRO_TEMPERATURES_RAW)
      gyro_temperature_raw_pub_ = nh_.advertise<geometry_msgs::PointStamped> ("imu_raw/gyro_temperature", freq_);


   // gps
   if((mask_.gps & SBG_OUTPUT_GPS_POSITION) || (mask_.gps & SBG_OUTPUT_GPS_INFO))
      gps_pub_ = nh_.advertise<sensor_msgs::NavSatFix>("navsat/fix", freq_);

   if(mask_.gps & SBG_OUTPUT_GPS_NAVIGATION)
      gps_velocity_pub_ = nh_.advertise<geometry_msgs::TwistWithCovarianceStamped>("navsat/vel", freq_);


   // barometric
   if(mask_.barometric & SBG_OUTPUT_BARO_PRESSURE)
      pressure_pub_ = nh_.advertise<sensor_msgs::FluidPressure>("pressure", freq_);


   // Kalman filter
   if((mask_.kf & SBG_OUTPUT_POSITION) || (mask_.kf & SBG_OUTPUT_VELOCITY) || (mask_.kf & SBG_OUTPUT_HEAVE) || 
      (mask_.kf & SBG_OUTPUT_ATTITUDE_ACCURACY) || (mask_.kf & SBG_OUTPUT_NAV_ACCURACY))
      odometry_pub_ = nh_.advertise<nav_msgs::Odometry>("odom", freq_);
   

   // other
   if(mask_.other & SBG_OUTPUT_DEVICE_STATUS)
      status_pub_ = nh_.advertise<sbg_ig500n::IGStatus>("status", freq_);


   // time
   if(mask_.time & SBG_OUTPUT_TIME_SINCE_RESET)
      time_pub_ = nh_.advertise<sensor_msgs::TimeReference>("time", freq_);

   if(mask_.time & SBG_OUTPUT_UTC_TIME_REFERENCE)
      utc_time_pub_ = nh_.advertise<sensor_msgs::TimeReference>("navsat/time", freq_);
}

