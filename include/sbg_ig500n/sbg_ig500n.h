/** 
 *  \file    sbg_ig500n.h
 *  \brief   Implementation of ROS node that interfaces with an SBG Systems IG-500N.
 *
 *  \author Emili Hernandez
 *  \date   March 2014
 *  \version $Revision $
 *
 *  CSIRO Autonomous Systems Laboratory
 *  Queensland Centre for Advanced Technologies
 *  PO Box 883, Kenmore, QLD 4069, Australia
 *
 *  (c) Copyright CSIRO 2014
 *
 *  All rights reserved, no part of this program may be used
 *  without explicit permission of CSIRO
 *
 */

#include "ros/ros.h"

#include "diagnostic_msgs/DiagnosticStatus.h"
#include "diagnostic_updater/diagnostic_updater.h"
#include "diagnostic_updater/update_functions.h"
#include "diagnostic_updater/DiagnosticStatusWrapper.h"

#include "std_msgs/Int32.h"
#include "sensor_msgs/Imu.h"
#include "sensor_msgs/NavSatFix.h"
#include "sensor_msgs/Temperature.h"
#include "sensor_msgs/TimeReference.h"
#include "sensor_msgs/MagneticField.h"
#include "sensor_msgs/FluidPressure.h"
#include "geometry_msgs/PointStamped.h"
#include "geometry_msgs/TwistWithCovarianceStamped.h"
#include "nav_msgs/Odometry.h"
#include <sbg_ig500n/IGStatus.h>
#include "tf/tf.h"

#include <vector>
#include <fstream>
#include <iostream>
#include <ctime>
#include <bitset>

namespace sbg
{
   #include <sbgCom.h>
   const double PI = 3.14159265359;
  
   struct OutputMask
   {
      sbg::uint32 sensors;
      sbg::uint32 sensors_raw;
      sbg::uint32 gps;
      sbg::uint32 kf;
      sbg::uint32 barometric;
      sbg::uint32 time;
      sbg::uint32 other;   
   };
}


class SbgIG
{
public:
   SbgIG(ros::NodeHandle h);
   ~SbgIG();
   
   std::string getInfo(); 
   int spin();
 
   
private:
   unsigned int new_data_;
   bool update_gps_;
   bool running_;

   sbg::SbgProtocolHandle protocol_handle_;
   sbg::SbgErrorCode error_;
   sbg::SbgOutput output_;
   sbg::OutputMask mask_;
    
  
   sensor_msgs::Imu imu_reading_;
   sensor_msgs::Temperature t0_reading_, t1_reading_;
   sensor_msgs::MagneticField magnetometer_reading_;
   geometry_msgs::PointStamped gyro_temperature_reading_;

   sensor_msgs::Imu imu_raw_reading_;
   sensor_msgs::Temperature t0_raw_reading_, t1_raw_reading_;
   sensor_msgs::MagneticField magnetometer_raw_reading_;
   geometry_msgs::PointStamped gyro_temperature_raw_reading_;

   nav_msgs::Odometry odometry_reading_;
   
   sensor_msgs::NavSatFix gps_reading_;
   geometry_msgs::TwistWithCovarianceStamped gps_velocity_reading_;

   sensor_msgs::FluidPressure pressure_reading_;

   sbg_ig500n::IGStatus status_reading_;
   sensor_msgs::TimeReference time_since_reset_reading_;
   sensor_msgs::TimeReference utc_time_reading_;

   ros::NodeHandle nh_;
   ros::NodeHandle nh_private_;

   double angular_velocity_stdev_, linear_acceleration_stdev_, orientation_stdev_;
   double xy_stdev_, altitude_stdev_ ;

   std::string port_;
   int divider_;

   std::string mpFolder_;

   ros::Publisher imu_pub_;
   ros::Publisher t0_pub_, t1_pub_;
   ros::Publisher magnetometer_pub_;
   ros::Publisher gyro_temperature_pub_;

   ros::Publisher imu_raw_pub_;
   ros::Publisher t0_raw_pub_, t1_raw_pub_;
   ros::Publisher magnetometer_raw_pub_;
   ros::Publisher gyro_temperature_raw_pub_;

   ros::Publisher gps_pub_;
   ros::Publisher gps_velocity_pub_;

   ros::Publisher pressure_pub_;

   ros::Publisher odometry_pub_;
   ros::Publisher status_pub_;
   ros::Publisher time_pub_;
   ros::Publisher utc_time_pub_;

   diagnostic_updater::FrequencyStatus *freq_diag_;
   diagnostic_updater::Updater diagnostic_;


   int error_count_;
   int slow_count_;
   std::string was_slow_;
   std::string error_status_;

   std::string frame_id_;
   std::string imu_topic_;

   double desired_freq_;
   int freq_;
   
   

   std::string device_code_;
   std::string mp_filename_;
   uint32_t mp_id_, mp_version_;

   static void triggerErrorCallback(sbg::SbgProtocolHandleInt *pHandler, sbg::SbgErrorCode error, void *pUsrArg);

   static void triggerCB(sbg::SbgProtocolHandleInt *handle, sbg::uint32 triggerMask, sbg::SbgOutput *output_ptr, void *arg_ptr);
   
   int formatDatum(sbg::SbgOutput *data);
   int publishDatum();
   void fillIGStatus(const uint32_t status);


   int start();
   void stop();
   
   sbg::SbgErrorCode connect(const std::string port, sbg::SbgProtocolHandle *handle);
   std::string error_code_str(sbg::SbgErrorCode error) const;
   std::string versionToString(const sbg::uint32 version);

   int getMPID(const std::string s);
   
   
   void setMotionProfile(const std::string filename);   
   void deviceStatus(diagnostic_updater::DiagnosticStatusWrapper &status);

   void loadOutputMask(sbg::OutputMask& m);
   void setupPublishers();
};
