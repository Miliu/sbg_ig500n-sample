#sbg_ig500n
------------------

ROS diver and utils for the SGB IG500N AHRS from SBG Systems. 

**This is a code sample only, it will not compile. SBG API and Git hystory have been removed from this package.**

------------------
##Instructions

- **sbg_ig500n_node** implements the driver 
	- Imu data is returned in NED coordinates (unless changed with an orientation matrix using SbgCenter)
	- GPS data in WSG84 projection
	- SBAS CANNOT be deactivated according to SBG systems (THE MANUAL IS WRONG!)
	- Odometry positon in UTM
	- Magnetometers in SBG "arbitrary units"
	- Output data can be specified at .yaml file. If not specified the default mask is:
		QUATERNION | GYROSCOPES | ACCELEROMETERS | MAGNETOMETERS | TIME_SINCE_RESET | DEVICE_STATUS;


- **ned2enu** node republishes imu data in ENU frame in a different topic. Frame is referenced as follows:
	- X_ENU = Y_NED
	- Y_ENU = X_NED
	- Z_ENU = -Z_NED

	![alt text](./ned_enu.png)



-------------------
###TODO
- Convert magnetometers data (SBG "arbitrary units") to Tesla
- Find imu orienation, linear accel, angular accel stdev (borrowed from GX2 
  driver)
 

###Revision Notes (01/10/2014)
- Only the minimum required publishers are activated

- Data collection occurs now with 2 triggers:
	  - SBG_TRIGGER_GPS_VELOCITY for GPS data only at lower update rate
	  - SBG_TRIGGER_MAIN_LOOP_DIVIDER for all other data 

- If the data is not streamed at the desired frequency, check 'Protocol' status. 
  If it's not ok the device baud-rate should be increased with SbgCenter.

